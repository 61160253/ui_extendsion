import 'package:flutter/material.dart';
import 'package:ui_extension/checkbox_widget.dart';
import 'package:ui_extension/checkboxtile_widget.dart';
import 'package:ui_extension/radio_widget.dart';

import 'dropdown_widget.dart';

void main() {
  runApp(MaterialApp(title: 'UI EXTENDSION', home: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('UI EXTENDSION')),
      drawer: Drawer(
        child: ListView(
          children: [
            const DrawerHeader(
              child: Text('UI MENU'),
              decoration: BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              title: Text('CHECKBOX'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CheckBoxWidget()));
              },
            ),
            ListTile(
              title: Text('CHECKBOX TILE'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CheckBoxTileWidget()));
              },
            ),
            ListTile(
              title: Text('DROPDOWN'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DropDownWidget()));
              },
            ),
            ListTile(
              title: Text('RADIO'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RadioWidget()));
              },
            )
          ],
        ),
      ),
      body: ListView(
        children: [],
      ),
    );
  }
}
