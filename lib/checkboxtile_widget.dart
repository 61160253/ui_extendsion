import 'package:flutter/material.dart';

class CheckBoxTileWidget extends StatefulWidget {
  CheckBoxTileWidget({Key? key}) : super(key: key);

  @override
  _ComboBoxTtileWidgeState createState() => _ComboBoxTtileWidgeState();
}

class _ComboBoxTtileWidgeState extends State<CheckBoxTileWidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
      appBar: AppBar(title: Text('ComboBoxTile')),
      body: ListView(
        children: [
          CheckboxListTile(
              title: Text('CHECK1'),
              subtitle: Text('SUB CHECK1'),
              value: check1,
              onChanged: (value) {
                setState(() {
                  check1 = value!;
                });
              }),
          CheckboxListTile(
              title: Text('CHECK2'),
              subtitle: Text('SUB CHECK2'),
              value: check2,
              onChanged: (value) {
                setState(() {
                  check2 = value!;
                });
              }),
          CheckboxListTile(
              title: Text('CHECK3'),
              subtitle: Text('SUB CHECK3'),
              value: check3,
              onChanged: (value) {
                setState(() {
                  check3 = value!;
                });
              }),
          TextButton.icon(
              onPressed: () {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(
                        'CHECK1: $check1,CHECK2: $check2,CHECK3: $check3')));
              },
              icon: Icon(Icons.save),
              label: Text('SAVE'))
        ],
      ),
    ));
  }
}
