import 'package:flutter/material.dart';

Widget _checkBox(String title, bool value, ValueChanged<bool?>? onChanged) {
  return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            children: [
              Text(title),
              Expanded(child: Container()),
              Checkbox(value: value, onChanged: onChanged)
            ],
          ),
          Divider()
        ],
      ));
}

class CheckBoxWidget extends StatefulWidget {
  CheckBoxWidget({Key? key}) : super(key: key);

  @override
  _CheckBoxWidgetState createState() => _CheckBoxWidgetState();
}

class _CheckBoxWidgetState extends State<CheckBoxWidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('ComboBox')),
      body: ListView(
        children: [
          _checkBox('CHECK 1', check1, (value) {
            setState(() {
              check1 = value!;
            });
          }),
          _checkBox('CHECK 2', check2, (value) {
            setState(() {
              check2 = value!;
            });
          }),
          _checkBox('CHECK 3', check3, (value) {
            setState(() {
              check3 = value!;
            });
          }),
          TextButton(
            child: Text('SAVE'),
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content:
                      Text('CHECK1: $check1,CHECK2: $check2,CHECK3: $check3')));
            },
          )
        ],
      ),
    );
  }
}
